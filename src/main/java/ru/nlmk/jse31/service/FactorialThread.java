package ru.nlmk.jse31.service;

import java.math.BigInteger;

public class FactorialThread extends Thread{

  private long startN;
  private long endN;
  private BigInteger partFactorial;

  public FactorialThread(long startN, long endN) {
    this.startN = startN;
    this.endN = endN;
  }

  @Override
  public void run()
  {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    this.partFactorial = Service.factorialRange(startN, endN);
  }

  public BigInteger getPartFactorial() {
    return this.partFactorial;
  }
}


