package ru.nlmk.jse31.service;

import com.google.common.math.LongMath;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigInteger.*;

public class Service {

  public int getFibonacciValue(int n) {

    if (n <= 1) {
      return 0;
    } else if (n == 2) {
      return 1;
    } else  {
      return getFibonacciValue(n - 1) + getFibonacciValue(n - 2);
    }
  }

  public long sum(String arg1, String arg2){

    if ((!arg1.matches("^-?\\d+$")) || (!arg1.matches("^-?\\d+$")))
      throw new IllegalArgumentException();

    return Long.parseLong(arg1) + Long.parseLong(arg2);
  }

  public long factorial(String arg){
    if ((!arg.matches("^-?\\d+$")) || (Long.parseLong(arg) < 0))
      throw new IllegalArgumentException();

    return LongMath.factorial(Integer.parseInt(arg));
  }

  public long[] fibonacci(String arg) {
    if ((!arg.matches("^-?\\d+$")) || (Long.parseLong(arg) < 1))
      throw new IllegalArgumentException();

    long num = Long.parseLong(arg);
    long sum = 0;
    long n = Long.MAX_VALUE;
    int nequal = -1;
    long result[];

    for (int i = 1; i < n; i++){
      sum += getFibonacciValue(i);

      if (num == sum){
        nequal = i;
        break;
      } else if (sum > num){
        break;
      }
    }

    if (nequal == -1)
      throw new IllegalArgumentException();
    else {
      result = new long[nequal];
      
      for (int i = 0; i < nequal; i++){
        result[i] = getFibonacciValue(i + 1);
      }
      return result;
    }  
  }

  public static BigInteger factorialRange(long startN, long endN){
    BigInteger result;
    result = valueOf(1);

    for (long i = startN; i <= endN; i++) {
      if (i == 0)
        result = valueOf(1);
      else {
        result = result.multiply(valueOf(i));
      }
    }

    return result;
  }

  public BigInteger factorial(String arg, int threadsCount){
    long startN, endN;
    int rest;
    FactorialThread factorialThread;
    List<FactorialThread> listFactorialThread = new ArrayList<>();
    BigInteger result = BigInteger.valueOf(1);

    if ((!arg.matches("^-?\\d+$")) || (Long.parseLong(arg) < 0))
      throw new IllegalArgumentException();

    long n = Long.parseLong(arg);
    if (n <= 14){
      threadsCount = 1;
      rest = 0;
    }
    else {
      rest = (int) (n % threadsCount);
    }

    //System.out.println("threadsCount = " + threadsCount);
    //System.out.println("rest = " + rest);

    for (long i = 1; i <= threadsCount; i++) {
      if (i == 1){
        startN = 0;
        endN = (int) (n / threadsCount);
      }
      else if (i == threadsCount){
        startN = ((int) (n / threadsCount)) * (i - 1) + 1;
        endN = ((int) (n / threadsCount)) * i + rest;
      }
      else {
        startN = ((int) (n / threadsCount)) * (i - 1) + 1;
        endN = ((int) (n / threadsCount)) * i;
      }
      //System.out.println("startN = " + startN);
      //System.out.println("endN = " + endN);

      factorialThread = new FactorialThread(startN, endN);
      listFactorialThread.add(factorialThread);
      factorialThread.start();

    }

    try {
      Thread.sleep(4000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    for (int i = 0; i < threadsCount; i++) {

      if (listFactorialThread.get(i).isAlive()){
        try{
          listFactorialThread.get(i).join();	//Подождать пока работа закончится.
        }catch(InterruptedException e) {
          ;
        }
      }

      result = result.multiply(listFactorialThread.get(i).getPartFactorial());
      //System.out.println("result = " + result);
    }

    return result;
  }




}

