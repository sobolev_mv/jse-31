package ru.nlmk.jse31;

import ru.nlmk.jse31.service.Service;

public class App {

  public static void main(final String[] args) {
    System.out.println("Считаем многопоточный факториал:");

    Service service = new Service();
    System.out.println(service.factorial("21", 4));
  }

}
